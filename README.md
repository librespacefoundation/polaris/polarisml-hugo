# PolarisML.space - Hugo code for new website

tl;dr:

- Uses [gohugo.io][1] for static site generation
- Uses https://themes.gohugo.io//theme/syna for theme (probably
  overkill, but decent layout)
- Hero image up at the top is from [Pexels.com][2] ([CC-0][3])

# To hack on this:

## Install hugo

- [Install hugo][0]

## Clone the repo

```
git clone --recurse-submodules [this repo]
cd [directory]
```

## Edit settings

**Make sure you edit config.toml** and set baseurl to just '/':

```
# Site settings
baseurl = "/" # Uncomment this line for local development
```

## Run test server

Then run:

```
hugo serve -D

```

[I think you can ignore error re: incompatible module for now]

- Site will be available at http://localhost:1313; should update
  automagically as needed

## Try editing files

Some things to play with:

- `/config.toml`
  - `[[menu]]` blocks control the links at the top

- `/content/_global` has various blocks that appear in different
  places

- `/content/_index/` - files here control the index page

- [Fragments][4] appear to be quite neat
  - Defined in `/layouts/partials/fragments`
  - Can be called in any page? I think?
  - See `/content/_index/polaris-intro-fragment.md` (and others in
    that directory) for examples

## Generating files for publishing

To generate static files:

- maybe change config.toml's baseurl if it's going to be published to
  my site

```
hugo && rsync --delete -avr public/. remote_server:/path/to/site/files/
```

## A note about git

I started this site by cloning [syna-start][5] and going from there.

[0]: https://gohugo.io/getting-started/installing
[1]: https://gohugo.io
[2]: https://www.pexels.com/photo/astronomy-atmosphere-earth-exploration-220201/
[3]: https://www.pexels.com/creative-commons-images/
[4]: https://themes.gohugo.io//theme/syna/docs/fragments/
[5]: https://github.com/okkur/syna-start
