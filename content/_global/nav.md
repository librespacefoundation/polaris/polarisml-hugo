+++
fragment = "nav"
#disabled = true
date = "2018-05-17"
weight = 0
#background = ""

[repo_button]
  url = "https://gitlab.com/librespacefoundation/polaris/polaris"
  text = "Star" # default: "Star"
  icon = "fab fa-gitlab" # defaults: "fab fa-github"

# Branding options
[asset]
  image = "polaris_logo.png"
  text = "Polaris"
+++
