+++
fragment = "hero"
#disabled = true
date = "2016-09-07"
weight = 50
background = "dark" # can influence the text color
particles = true

title = "PolarisML.space"
subtitle = "Polaris: Machine Learning for Satellite Operations"

[header]
  image = "pexels-pixabay-220201.jpg"

[asset]
  image = "polaris_logo.png"
  # width = "500px" # optional - will default to image width
  height = "150px" # optional - will default to image height

[[buttons]]
  text = "Demo"
  url = "https://polarisml.space/demo"
  color = "secondary" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary

[[buttons]]
  text = "Docs"
  url = "https://docs.polarisml.space/en/latest"

  color = "info" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary

[[buttons]]
  text = "Code"
  url = "https://gitlab.com/librespacefoundation/polaris/polaris"
  color = "primary"

[[buttons]]
  text = "Chat"
  url = "https://riot.im/app/#/room/#polaris:matrix.org"
  color = "success"
+++
